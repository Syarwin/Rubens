\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{subfig, graphicx}
\usepackage{fullpage}

\title{Documentation de la machine de Turing en Lego}



\begin{document}
	\maketitle


\section{Détails de la machine}
	\subsection{Le compresseur}
		La machine est \textbf{entièrement mécanique} puisqu'elle fonctionne uniquement à partir d'air comprimé.
		Bien qu'il soit possible de fournir cet air comprimé à l'aide d'une simple pompe à vélo, il est plus pratique d'utiliser un compresseur pour faire ce travail automatiquement. \\
		Le compresseur est réglé pour délivrer une pression d'environ \textbf{2 bar}, qui correspond à la pression maximale que peuvent subir les durites (tuyaux) utilisées sur la machine avant de se détacher.
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=0.3]{photos/compresseur.jpg}
			\caption{Photo du compresseur éteint}
		\end{figure}



	\subsection{Les moteurs}
		\`{A} plusieurs endroits de la machine, on a besoin de faire tourner des engrenages et des barres, et il faut donc transformer l'air comprimé en rotation.
		Cela est effectué à l'aide des deux moteurs de la machine qui fonctionnent selon un système similaire à ceux des moteurs à explosions : chaque moteur est composé de 3 pistons qui sont reliés à un seul axe à l'aide de bielles qui sont décalées de \textbf{1/3} les unes par rapport aux autres. 
		Comme on peut le voir sur la Figure \ref{Fig:moteur3cylindres}, lorsque un des pistons pousse (bleu), un autre est en train de tirer (orange), et le troisième est en fin de course (gris), puis les rôles se décalent au cours du mouvement.
		L’intérêt d'avoir 3 pistons est d'éviter les points morts et donc de fournir un mouvement de rotation continu sans à-coup.

	 	\begin{figure}[h]
			\centering
			\subfloat{{ \includegraphics[scale=0.5]{figures/moteur_3_cylindres_1.jpg} }}%
			\qquad
			\subfloat{{ \includegraphics[scale=0.5]{figures/moteur_3_cylindres_2.jpg} }}%
			\qquad
			\subfloat{{ \includegraphics[scale=0.5]{figures/moteur_3_cylindres_3.jpg} }}%
			\caption{Différentes configurations du moteur}
			\label{Fig:moteur3cylindres}
		\end{figure}
	
	
		Un des moteurs fonctionne en continu pour faire tourner les Boites à Musique (BàM) de l'ordonnanceur tandis que le deuxième moteur est activé uniquement lorsque la lecture est activé, et sert à effectuer un pas de calcul en fonction de la table de transition.
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=0.2]{photos/moteurs.jpg}
			\caption{Photo des deux moteurs de la machine}
		\end{figure}	
	
	
	
	\subsection{L'ordonnanceur}
		L'ordonnanceur organise les différentes étapes du cycle de la machine : lecture, écriture, déplacement, ...
		Il fonctionne sur le principe de la boîte à musique : deux cylindres composés de \textbf{10 barres jaunes} tournent en continu et vont actionner certains interrupteurs en fonction des picots qui seront positionnés sur les barres jaunes.
		Ces boites à musique sont reliées au moteur par une série d'engrenage qui ralentit la vitesse de rotation d'un facteur ?, pour au final une durée de cycle de \textbf{100 secondes} environ.
				
		\begin{figure}[h]
			\centering
			\includegraphics[scale=0.3]{photos/ordonnanceur.jpg}
			\caption{Photo de l'ordonnanceur avec ses deux boites à musique}
		\end{figure}
	
		Le comportement de la machine est donc modulable puisqu'il suffit de déplacer les picots pour modifier les actions qu'elle effectue.
		Cependant, afin que la machine fonctionne comme on le souhaite, il faut respecter certaines contraintes :
		\begin{itemize}
			\item Il faut arrêter de lire avant de commencer à écrire.
			\item Il faut finir l'écriture avant de se déplacer
			\item Avant le déplacement aller, il faut baisser les griffes.
			\item Avant le déplacement retour, il faut lever les griffes.
			\item Il faut lever les griffes pas trop longtemps après le début de la lecture : cf changement d'état.
			\item Il faut laisser assez de temps à la lecture pour que la transition s'effectue.
		\end{itemize}

		Plusieurs ordonnancement satisfaisant ces contraintes sont possibles, en voici un exemple sur la Figure \ref{Fig:ordonnancement}.
		
		\begin{figure}[h!]
			\centering
			\includegraphics[scale=0.9]{figures/ordonnancement.pdf}
			\caption{Ordonnancement de la machine sur un cycle de 10 unités}
			\label{Fig:ordonnancement}
		\end{figure}
	

	
	\subsection{L'état}
		L'état de la machine est conservé et mis à jour à l'aide d'un système de piston-levier : le levier sert à marquer dans quel état on se trouve et le piston permet de modifier l'état.
		Suivant la position du levier, l'air comprimé sortira par le trou de droite ou de gauche, ce qui permettra par la suite de sélectionner tel ou tel groupe de barres de la table de transition.
		
	 	\begin{figure}[h]
			\centering
			\subfloat{{ \includegraphics[scale=0.2]{figures/piston_levier_1.png} }}%
			\qquad
			\subfloat{{ \includegraphics[scale=0.2]{figures/piston_levier_2.png} }}%
			\caption{Système piston-levier de l'état}
		\end{figure}

		Pour mettre à jour le levier, il suffit d'envoyer de l'air au piston suivant le nouvel état voulu.
		Ce piston est donc directement connecté au levier du V de la table de transition correspondant à l'état.
		Ce système permet de retarder la mise à jour de l'état, ce qui est crucial car il ne faut pas que l'état se mette à jour pendant la lecture, puisque cela pourrait fausser la transition.
		Ainsi, la mise à jour de l'état est synchronisé avec les griffes qui se baissent, assurant ainsi que la lecture est terminé lorsque cela arrive.



	\subsection{La table de transition}
	
		\subsubsection{Le sélecteur}
			Le sélecteur a pour but de désactiver toutes les colonnes (barres verticales) de la table de transition en les soulevant sauf une, que l'on appellera \emph{barre activée} dans la suite.
			La sélection de la barre dépends de deux choses : \textbf{l'état} dans lequel se trouve la machine et ce qui est \textbf{lu sur le ruban}.

			\begin{figure}[h]
				\centering
				\includegraphics[scale=0.7]{photos/selecteur.jpg}
				\caption{Photo du sélecteur de la table de transition}
			\end{figure}

			Le système est composé de 6 pistons qui fonctionnent par paires : lorsqu'un des pistons de la paires pousse, l'autre piston de la paire tire.
			Lorsqu'un piston tire, il soulève un ensemble de barre qui sont reliées au piston par des tiges horizontales.
			Chaque paire de piston désactive ainsi la moitié des barres.
			

			\begin{figure}[h]
				\centering
				\includegraphics[scale=1]{figures/table.pdf}
				\caption{Schéma du sélecteur de la table de transition}
			\end{figure}
	
	
	
	\subsection{Le portique}
		\subsection{La lecture}	
		 	\begin{figure}[h]
				\centering
				\subfloat{{ \includegraphics[scale=0.6]{figures/lecture_1.png} }}%
				\qquad
				\subfloat{{ \includegraphics[scale=0.6]{figures/lecture_2.png} }}%
				\caption{Système de lecture}
			\end{figure}
\end{document}