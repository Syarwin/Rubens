\documentclass[12pt, oneside]{article}   	% use "amsart" instead of "article" for AMSLaTeX format
\usepackage[top=.9in,bottom=.9in]{geometry}                		% See geometry.pdf to learn the layout options. There are lots.

\usepackage[utf8]{inputenc}
\usepackage[frenchb]{babel}

\geometry{a4paper}                   		% ... or a4paper or a5paper or ... 
%\geometry{landscape}                		% Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    		% Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}				% Use pdf, png, jpg, or eps with pdflatex; use eps in DVI mode
								% TeX will automatically convert eps --> pdf in pdflatex		
\usepackage{amssymb}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{mathptmx}
\usepackage[usenames,dvipsnames]{color}
\usepackage{array}
\usepackage{tikz}
\usepackage{gantt}
\usepackage{enumitem}

\newcommand{\TODO}[1]{{\color{red}TODO: #1}}

\title{
  \includegraphics[height=2cm]{ucbl-logo.jpg}\\
  \textcolor{Orange}{Appel à Projet \og PS-DCSTI 2014 \fg}\\[60pt]
  Dossier de candidature du projet\\[-5pt]
  \rule{10cm}{2pt}\\[18pt]
  \resizebox{12cm}{!}{RubENS@Chile}
  \rule{10cm}{2pt}\\[50pt]
}
\author{\begin{tabular}{>{\color{Orange}}rl}
  Porteur du projet : & Eddy Caron\\
  Laboratoire : & \'ENS de Lyon (LIP)\\
  Adresse mail : & \url{Eddy.Caron@ens-lyon.fr}\\
  Adresse professionnelle : & $\!\!\!$\begin{tabular}[t]{l}LIP\\46 Allée d'Italie\\69364 Lyon Cedex 7\end{tabular}\\
  Téléphone : & 04.72.72.80.04\\
  Statut : & Enseignant-chercheur
\end{tabular}\\[150pt]}
\date{
  \mbox{
    \phantom{.}\hspace{-2.2cm}
    \raisebox{-7pt}{\includegraphics[width=3cm]{ucbl-logo-small.jpg}}~~~~
    \includegraphics[width=3.5cm]{ens-logo.png}~~~~~~
    \raisebox{12pt}{\includegraphics[width=4cm]{rubens-crop.png}}~~~~~~
    \includegraphics[width=5cm]{uai-logo.png}
  }
}							% Activate to display a given date or no date

\begin{document}

\setlist[itemize]{leftmargin=16pt}
\setlist[enumerate]{leftmargin=16pt}

%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
%%
%% PAGE DE GARDE
%%
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%

\maketitle

\thispagestyle{empty}

\newpage

%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
%%
%% PROJET
%%
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%

\setcounter{page}{1}

\section{Objectifs du projet}

%%%%%%%%%%%%%%%%%%%%
%%
%% CONTEXTE
%%
%%%%%%%%%%%%%%%%%%%%

\subsection{Contexte : qu'est-ce que le projet RubENS ?}

\begin{wrapfigure}{r}{8.5cm}
  \vspace{-12pt}
  \includegraphics[width=10cm]{rubens-machine.jpg}
  \vspace{-22pt}
\end{wrapfigure}

Une machine de Turing est une machine abstraite imaginée par Alan Turing en 1936, avant que le premier ordinateur n'ait été construit. Rien de plus normal, car cette machine est la première idée mathématique ---et toujours la référence--- capturant le concept de {\em calcul}, et la mère de tous les ordinateurs modernes.

En 2012, pour célébrer le centenaire d'Alan Turing (1912-1954), un groupe d'étudiants de l'\'Ecole Normale Supérieure de Lyon a construit une machine de Turing en briques de Lego{\scriptsize \texttrademark}, sans aucune utilisation de composant électronique. Cette machine est entièrement mécanique : l'unique source d'énergie utilisée, de l'air comprimé à 2 bars, est fournie par une pompe à vélo. Rien n'est caché, et l'on peut pour la première fois {\em voir} un ordinateur calculer !

Bien sûr, il y a une légère contrepartie\dots\, Cette machine est réellement capable de faire tout ce qu'un smartphone dernier cri peut faire, mais pour effectuer ce que ce dernier réalise en 1 seconde, notre machine de Turing en Lego{\scriptsize \texttrademark} nécessite 3168 ans, 295 jours, 9 heures, 46 minutes et 40 secondes. Quoi qu'il en soit, l'important est qu'elle soit {\em capable} de le faire, n'est-ce pas ?

Cet objet est un support très attractif pour la vulgarisation scientifique sur le thème de la {\em calculabilité}, que nous avons eu le plaisir d'exposer au cours d'évènements nationaux et internationaux : expositions (Conférence {\em Super Computing} 2013 à Denver aux \'Etats-Unis, en 2013 à la mairie du \textsc{V}$^e$ de Paris et au bâtiment Nautibus de l'Université Claude Bernard, au {\em Journées Turing} 2012 à l'\'ENS de Lyon), articles dans la presse ({\em Le Monde} éditions papier et en ligne, {\em Le Progrès}, ainsi qu'un article en cours de révision pour le site {\em Images des Maths} du CNRS), et dans des documentaires télévisés (film de présentation du CNRS\footnote{\mbox{\url{http://www.dailymotion.com/video/xrmfie_the-turing-machine-comes-true_tech}.}}, la machine va également apparaître dans des documentaires indépendants et pour la chaîne {\em Arte}).

Le projet RubENS rencontre un grand intérêt dans la communauté informatique française, qui s'étend au delà de nos frontières. Kévin Perrot, membre du projet actuellement en postdoctorat à Santiago du Chili, a rencontré des responsables des départements d'informatique et de design de l'Université Adolfo Ibañez, qui souhaitent reproduire cette construction dans leur pays, pour promouvoir la recherche fondamentale.

Notre machine est le fruit d'un investissement sincère de chacun des membres du projet, qui a vu le jour après de nombreux essais et corrections, et réassembler les 20\,000 pièces qui la composent demandera un travail important. C'est pourquoi nous sollicitons l'aide du groupe de travail PS-DCSTI.

\newpage

%%%%%%%%%%%%%%%%%%%%
%%
%% OBJECTIFS
%%
%%%%%%%%%%%%%%%%%%%%

\subsection{Objectifs du projet : construire une nouvelle machine de Turing en Lego{\scriptsize \texttrademark} à Santiago !}

\subsubsection{Améliorer la robustesse de la construction}

Le succès rencontré par notre machine dépasse nos espérances initiales, et nos capacités de diffusion seraient grandement élargies par l'amélioration de la robustesse de la construction. Notre construction est délicate, son fonctionnement et son transport entraînent régulièrement quelques déconvenues. Par conséquent, il est à l'heure actuelle indispensable qu'un membre du projet soit présent pour que la machine puisse être actionnée, et cela représente souvent la plus grosse contrainte dans l'organisation des nombreux évènements pour lesquels nous sommes sollicités. Le premier objectif de cette proposition est ainsi de renforcer la fiabilité et la robustesse de la construction, ce qui demandera un certain temps et une certaine maîtrise.

\subsubsection{Créer un kit de diffusion.}

Une fois la première étape complétée, vient la partie du projet qui sera certainement la plus valorisée par la suite : créer un kit de diffusion. Il sera composé d'une liste des pièces utilisées, une notice de montage et de fonctionnement, ainsi qu'un ensemble d'éléments de vulgarisation (documentations, posters, brochures). Ce kit sera un support de diffusion de notre expertise, dont nous constatons qu'elle est très demandée. Il permettra d'essaimer des projets de vulgarisation analogues, pour lesquels nous ambitionnons le même succès. Intervient alors la troisième étape du projet RubENS@Chile\dots

\subsubsection{Construire une machine de Turing en Lego{\scriptsize \texttrademark} à Santiago.}

Les étapes précédentes seront concrétisées par la construction d'une nouvelle machine de Turing en Lego{\scriptsize \texttrademark} à l'Université Aldolfo Ibañez de Santiago, qui sera gérée par groupe d'étudiants locaux initiant un projet \og ami \fg\, indépendant. Ainsi, nous construirons à la fois un nouveau vecteur de diffusion scientifique, comme l'est le projet RubENS, et donnerons de la visibilité au kit de diffusion, par une première expérience qui appellera des reproductions. Le choix de l'Université Adolfo Ibañez est guidé par l'ambition assumée de notre projet de vulgarisation, dont le sujet est universel et donc international. La présence d'un membre du projet à Santiago a en outre permis de faciliter l'amorce des échanges, et assurera la continuité des relations avec le Chili.\\

Le projet RubENS@Chile sera doublement gagnant : le projet français en ressortira avec une meilleure machine, qui lui permettra de diffuser plus largement la culture scientifique en France, et le projet initié au Chili trouvera un fonctionnement indépendant et la liberté de promouvoir lui-aussi la recherche fondamentale de son côté de l'Atlantique. N'oublions pas le kit de diffusion, dont nous sommes convaincus de l'utilité, car il nous a plusieurs fois été demandé, notamment par les organisateurs d'évènements pour lesquels nous n'avons pas pu déplacer l'unique construction, lyonnaise.

\newpage

%%%%%%%%%%%%%%%%%%%%
%%
%% ACTIONS
%%
%%%%%%%%%%%%%%%%%%%%

\section{Actions}

\subsection{Organisation}

La clé de voute de ce programme consistera en l'accueil, au sein du projet RubENS à Lyon et pour une durée de 3 à 6 mois, d'un\footnote{Sans aucune intention discriminatoire et par seul souci d'alléger le présent texte, la forme masculine est utilisée comme genre neutre et désigne à la fois les femmes et les hommes.} stagiaire de l'Universidad Adolfo Ibañez. Ce dernier pourra alors étudier, avec le soutien des membres du projet RubENS, la construction en Lego{\scriptsize \texttrademark}. Il s'agira dans un premier temps de la comprendre et d'améliorer sa robustesse (au transport et au fonctionnement), puis dans un second temps de participer à la modélisation informatique de la machine, qui servira de base à l'édition d'un manuel d'instructions décrivant la procédure à suivre pour reproduite, pièce par pièce, la machine.

Notre construction en Lego{\scriptsize \texttrademark} est complexe, et ses mécanismes font l'objet de fré\-quentes discussions entre les membres du projet RubENS. Cette rencontre physique de 3 à 6 mois semble une étape incontournable dans ce premier projet de reproduction de la machine, et la venue d'une personne extérieure nous permettra de plus d'envisager le kit de diffusion sous un angle objectif. Ce stage sera en outre l'occasion de nouer un contact fort avec le futur projet chilien, et d'assurer ainsi la continuité des échanges.

\subsection{Étapes}

\subsubsection{Concours}

Il s'agira dans un premier temps d'élire un stagiaire parmi les étudiants de l'Université Adolfo Ibañez de Santiago. Pour catalyser l'engagement des candidats et initier la propagande autour du futur projet chilien, cette sélection se fera par un concours au sein des départements d'Informatique et de Design. Il s'agira d'élire un groupe de 3 à 8 étudiants, qui formeront le futur projet RubENS@Chile, et dont l'un effectuera le stage en France. \'Etant sur place, Kévin Perrot sera chargé de l'organisation pratique de cette compétition, dont les modalités restent à préciser. Les principaux critères de sélection seront la motivation, le sérieux, et l'habilité à ``réfléchir Lego{\scriptsize \texttrademark}''.

\subsubsection{Stage}

Le stage, encadré au LIP de l'\'ENS de Lyon par Eddy Caron, Yannick Leo et Florent Robic, se déroulera en quatre temps autour de la machine : découverte, amélioration, modélisation, création du kit de diffusion.

\begin{enumerate}
  \item Le stagiaire apprendra tout d'abord le fonctionnement de la machine, ses atouts, ses défauts. Une attention particulière sera portée aux pièces s'étant déjà détachées durant le transport.
  \item Il aura ensuite pour objectif de consolider la construction par le perfectionnement de certains mécanismes alors identifiés comme critiques, par leur fragilité de transport ou de fonctionnement.
  \item La modélisation informatique de la machine demandera du sérieux, de la concentration, et beaucoup de temps. Celle-ci n'a pas encore été réalisée pour cette dernière raison, et la venue d'un stagiaire à temps plein offrira l'opportunité d'accomplir cette tâche indispensable à l'élaboration d'un kit de diffusion. \'Epaulé par les membres du projet RubENS, il s'agira de sélectionner un logiciel de modélisation (SR 3D builder, LegoCad, ou Bricksmith), de le prendre en main, puis d'y construire la machine de Turing en Lego{\scriptsize \texttrademark}.
  \item \`A partir de la modélisation informatique de la machine, une notice de montage sera éditée, accompagnée d'une liste des pièces nécessaires. Un bon compromis entre clarté et volume du livret devra être atteint, pour l'intégrer à un ensemble d'éléments de vulgarisation autour de la machine de Turing en Lego{\scriptsize \texttrademark}, qui constituera le kit de diffusion. Ce dernier sera dans un premier temps traduit en français, anglais, et espagnol, jusqu'à ce que de nouvelles volontés s'expriment.
\end{enumerate}

\subsubsection{Machine santiaguina}

De retour à l'Université Adolfo Ibañez à Santiago avec le kit de diffusion en poche, l'étudiant pourra alors lancer le projet RubENS@Chile qui consistera en la construction d'une nouvelle machine de Turing en Lego{\scriptsize \texttrademark}, et sa valorisation par des évènements de vulgarisation scientifique. La commande des pièces pourra s'effectuer sur Internet via les vendeurs du marché {\em BrickLink} (en particulier {\em 1001 Bricks}), dont nous avons à plusieurs reprises constaté le sérieux, et où la livraison internationale est proposée. Un projet RubENS@Chile indépen\-dant sera formé en amont, au moment de la sélection du coucous, par un groupe d'étudiants de l'Universidad Adolfo Ibañez. Cette anticipation a pour but de commander les pièces dès que la modélisation informatique des Lego{\scriptsize \texttrademark}, qui permettra d'en établir une liste complète, sera terminée. La nouvelle machine de Turing en Lego{\scriptsize \texttrademark} sera assemblé à Santiago, pour y promouvoir la recherche fondamentale. Nous comptons sur cette première mise à profit du kit de diffusion, que suivra de près le projet lyonnais, pour appeler des reproductions et transmettre plus largement notre passion pour l'informatique théorique, à l'aide d'un objet merveilleusement attractif.

\subsection{Co-Financement}

Le coût de cette proposition est évalué à 10\,000 euros, pour moitié co-financé par le Chili, comme l'atteste la lettre du doyen de la Faculté des Sciences et de l'Ingénierie de l'Université Adolfo Ibañez ci-jointe. Les détails de cette évaluation sont les suivants.
\begin{center}
  \begin{tabular}{rcr}
    Billet d'avion A/R Lyon-Santiago &:& 1\,500 euros\\
    Gratification pour un stage de 6 mois &:& 6\,000 euros\\
    Pièces de Lego{\scriptsize \texttrademark} &:& 2\,000 euros\\
    Charges diverses de fonctionnement &:& 500 euros\\[-5pt]
    %&& $\overline{\mbox{10\,000 euros}}$
  \end{tabular}
\end{center}

\section{Calendrier}

Note sur le rythme scolaire de l'hémisphère Sud : les mois de Juillet et Aout n'y sont pas synonymes de vacances, et la rentrée scolaire a lieu à la mi-Mars.\\[-9pt]

%\begin{center}
  \ \hspace{-1.5cm}\begin{tabular}{rcp{10cm}}
    Juillet 2014 &:& Mise en place du concours à l'Université Adolfo Ibañez de Santiago, par Kévin Perrot.\\[2pt]
    1 Aout 2014 &:& \'Election du stagiaire.\\[2pt]
    Sept 2014 - Févr 2015 &:& Stage au LIP de l'\'ENS de Lyon, encadré par Eddy Caron, Yannick Leo et Florent Robic.\\[2pt]
    1-15 Mars 2015 &:& Construction d'une nouvelle machine de Turing en Lego{\scriptsize \texttrademark} à l'Université Adolfo Ibañez, et première présentation à la rentrée scolaire pour une visibilité maximale.%\\[10pt]
  \end{tabular}
%\end{center}

Ces éléments sont repris dans le diagramme de Gantt suivant. Notons que le kit de diffusion constituera pour une grande part le rapport de stage de l'étudiant, et que nous avons déjà construit des posters, brochures, et documentations, sur lesquels nous pourrons nous baser.

%%PACKAGE gantt
\begin{gantt}[xunitlength=1cm,fontsize=\small,titlefontsize=\small,drawledgerline=true]{19}{10}
  \begin{ganttitle}
   \titleelement{2013}{7}
   \titleelement{2014}{3}
  \end{ganttitle}
  \begin{ganttitle}
   \numtitle{6}{1}{12}{1}
   \numtitle{1}{1}{3}{1}
  \end{ganttitle}
  %Concours
  \ganttgroup{Concours}{.5}{1.5}
  \ganttbar{Préparation}{.5}{.5}
  \ganttbarcon{Déroulement}{1}{1}
  \ganttbarcon{Résultat}{2}{.1}
  %Stage
  \ganttgroup{Stage}{3}{6}
  %\ganttcon{2}{5}{3}{7}
  \ganttbar{Découverte}{3}{.25}
  \ganttbarcon{Amélioration}{3.25}{1}
  \ganttbarcon{Modélisation}{4.25}{2.5}
  \ganttbarcon{Notice de Montage}{7}{1}
  \ganttbar{\textbf{Rapport de stage}}{8.5}{.5}
  %Kit diffusion
  \ganttgroup{Kit de diffusion}{3}{6}
  \ganttbar{Posers, Brochures}{3}{5}
  \ganttcon{8}{10}{8}{14}
  \ganttbarcon{Finalisation}{8}{.5}
  %RubENS@Chile
  \ganttgroup{RubENS@Chile}{2}{8}
  \ganttcon{6.75}{9}{6.75}{16}
  \ganttbar{Commande des pièces}{6.75}{.25}
  \ganttbarcon{Assemblage}{9}{.5}
  \ganttbarcon{Inauguration}{9.5}{.5}
\end{gantt}

%%PACKAGE pgfgantt
%\begin{ganttchart}[y unit title=0.4cm,
%y unit chart=0.5cm,
%vgrid,hgrid, 
%title label anchor/.style={below=-1.6ex},
%title left shift=.05,
%title right shift=-.05,
%title height=1,
%bar/.style={fill=gray!50},
%incomplete/.style={fill=white},
%progress label text={},
%bar height=0.7,
%group right shift=0,
%group top shift=.6,
%group height=.3,
%group peaks={}{}{.2}]{10}
%  \gantttitle{2013}{7}
%  \gantttitle{2014}{3}
%  \gantttitle{Juin}{1}
%  \gantttitle{Juil}{1}
%  \gantttitle{Aout}{1}
%  \gantttitle{Sept}{1}
%  \gantttitle{Octo}{1}
%  \gantttitle{Nove}{1}
%  \gantttitle{Déce}{1}
%  \gantttitle{Janv}{1}
%  \gantttitle{Févr}{1}
%  \gantttitle{Mars}{1}
%  %Concours
%%  \ganttgroup{Concours}{.5}{1.5}
%  \ganttbar{Préparation}{.5}{.5}
%  \ganttbar{Déroulement}{1}{1}
%  \ganttbar{Résultat}{2}{.1}
%  %Stage
%  \ganttgroup{Stage}{3}{6}
%  %\ganttcon{2}{5}{3}{7}
%  \ganttbar{Découverte}{3}{.25}
%  \ganttbar{Amélioration}{3.25}{1}
%  \ganttbar{Modélisation}{4.25}{2.75}
%  \ganttbar{Notice de Montage}{7}{1}
%  \ganttbar{\textbf{Rapport de stage}}{8.5}{.5}
%  %Kit diffusion
%%  \ganttgroup{Kit de diffusion}{3}{6}
%  \ganttbar{Posers, Brochures}{3}{5}
%%  \ganttcon{8}{10}{8}{14}
%  \ganttbar{Finalisation}{8}{.5}
%  %RubENS@Chile
%%  \ganttgroup{RubENS@Chile}{2}{8}
%  \ganttbar{Commande des pièces}{7}{1}
%%  \ganttcon{7}{9}{7}{16}
%  \ganttbar{Assemblage}{9}{.5}
%  \ganttbar{Présentation}{9.5}{.5}
%\end{ganttchart}

%\TODO{C'est quand les stages au Chili ???????????????}

\section{Lieux du projet}

Le concours de recrutement du stagiaire sera mis en place à l'Université Adolfo Ibañez de Santiago, au Chili.

Le stage et les interactions avec les membres du projet RubENS auront lieu à l’École Normale Supérieure de Lyon. L’étudiant sera hébergé au sein du LIP au travers d’une convention d’accueil de stage classique.

L'assemblage d'une nouvelle machine de Turing en Lego{\scriptsize \texttrademark} se fera dans les locaux du département de Design de l'Université Adolfo Ibañez.

\end{document}
